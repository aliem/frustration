(function ($) {
  var defaults = {
      class: "lava"
    , duration: 250
    , zIndex: 10
    , active: '.active'
    , complete: function(){}
    };
  
  function LavaLamp (target, opts) {
    this.t = $(target);
    this.o = $.extend(defaults, opts);
    
    if (this.o.active.$ == 'undefined') this.o.active = $(this.o.active, this.t);

    this.t.css({
      'position': 'relative'
    });
    this.l = $('<div>').attr({
      class: this.o.class
    }).css({
      left: '0px'
    , 'position': 'absolute'
    }).appendTo(this.t);

    this.l.addClass('animated');

    this.move($(this.o.active, this.t));
    this.addEvents();
  }

  LavaLamp.prototype = {
    addEvents: function () {
      var self = this;
      $('a', this.t).on('mouseover', function (e) {
        self.move($(e.target));
      }).on('mouseout', function (e) {
        self.move($(self.o.active, self.t));
      });
    }
  , move: function (tar) {
    var self = this;
    // this.l.animate(
    //   (tar.length)?
    //   {
    //       width: tar.offset().width + 'px' || '0px'
    //     //, left: this.t.offset().left - tar.offset().left
    //     , left: tar.offset().left - this.t.offset().left
    //     , duration: this.o.duration
    //     , complete: function () {
    //         self.o.complete(tar, self);
    //       }
    //   }
    //   :
    //   {
    //       width: 0
    //     , left: 0
    //     , duration: this.o.duration
    //     , complete: function () {
    //         self.o.complete(tar, self);
    //       }
    //   }
    // );
    // 
      this.l.css(
        (tar.length)
        ? {
            width: tar.offset().width + 'px' || '0px'
          , left: tar.offset().left - this.t.offset().left
          }
        : {
            width: 0
          , left: 0
          }
        )
      
    }
  };

  $.ender({
    lavalamp: function (options) {
      var ld = $(this).data('lavaLamp');
      if (options === 'lava' && ld) return ld;
      else if (options === 'lava') return $(this).data('lavaLamp', (new LavaLamp(this, options)));
      else $(this).data('lavaLamp', (new LavaLamp(this, options)));
      return this;
    }
  }, true);
})(window.ender);