(function ($) {
  $.ender({
    lazyload: function (delay, cb) {
        delay = parseInt(delay, 10) || 1000;
        cb = 'function' === typeof cb ? cb : function () {};
        var t = $(this)
          , c = t.length
          , n = 0
          , next = function () {
              if (n === c) return true;
              var o = n;
              setTimeout(function () {
                process(o);
              }, delay);
              n += 1;
            }
          , process = function (nn) {
              console.log(nn);
              var i = new Image()
                , el = $(t[nn]);
              i.onload = function (e) {
                el.attr('src', i.src);
                cb.call(el);
                next();
              };
              i.src = el.attr('data-href');
            };
        
        next();
        return this;
      }
  }, true);
})(window.ender);
