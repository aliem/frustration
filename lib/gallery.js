(function ($, window) {
  var _options = {
        duration: 250
      , change: function () {}
      }
    , v = require('valentine');
  
  function Gallery (list, options) {
    this.elements = list;
    this.target = options.target;
    if (!this.target) throw 'ERROR. target not found!';

    this.o = v.extend(_options, options);
    this._prepare();
    return this;
  }
  Gallery.prototype._prepare = function () {
    var self = this;
    this.elements.on('click', function(e){
      var t = $(this);
      e.preventDefault();

      self.change(t.attr('href'));
      return false;
    });
  };
  
  Gallery.prototype.change = function (link) {
    var img = new Image()
      , self = this;
    
    $(img)
      .hide()
      .on('load', function(e){
        self.o.target.css({opacity: 1}).animate({
          duration: self.o.duration
        , opacity: 0
        , complete: function() {
            self.o.target.attr('src', link).animate({
                opacity: 1
              , complete: function() {
                  self.o.change.call(self, link);
                }
            });
          }
        });
      })
      .attr('src', link);
    return this;
  };

  $.ender({
    gallery: function(options) {
      if ($(this).data('gallery'))
        return $(this).data('gallery');

      var dp = new Gallery(this, options);
      $(this).data('gallery', dp);
      return dp;
    }
  }, true);

  window.Gallery = Gallery;
})(window.ender, window);