(function($, window) {
  var defaults = {
        calId: 0
      , cssName: "default"
      , startDate: -1
      , endDate: -1
      , selectedDate: -1
      , showPrevNext: true
      , allowOld: true
      , showAlways: false
      , position: "absolute"
      }
    , v = require('valentine')
    , counter = 0;

  function DatePicker(target, options) {
    var self = this;

    this.id = 'datePicker'+ counter+1;
    counter += 1;

    this.t = $('target');
    this.s = v.extend(defaults, options);

    // Bind click and focus event to show
    this.t.bind('click focus', this.show);

    this.date = (typeof this.date == "undefined") ? startDate : this.date;

    // WTF!!!
    // If always showing, trigger click causing it to show
    //if(this.s.showAlways) {
    //  setTimeout(function() { self.trigger("focus"); }, 50);
    //}
  }

  DatePicker.prototype = {

    // Show the calendar
    show: function(e) {
      if (e) e.stopPropagation();
      if (this.shown) return this;


      // Bind click elsewhere to hide
      $(window).bind("click."+this.id, function () {
        self.hide();
        $(window).unbind("click."+self.id); // fixme for race conditions
      });
    }
  , hide: function(e) {
      if(!this.s.showAlways) return ;
      // Hide the calendar and remove class from target
      this.cal.animate({
        duration: 200
      , height: 0
      });
    }

    // Set a new start date
  , setStartDate: function (e) {
      this.t.startDate = e;
    }

    // Set a new end date
  , setEndDate: function (e) {
      this.t.endDate = e;
    }

    // Set a new selected date
  , setSelectedDate: function (e) {
      this.t.selectedDate = e;
    }

    // Render the calendar
  , update: function () {
      if (!this.startDate) {
        this.startDate = new Date();
        this.startDate.setHours(0,0,0,0);
      }
      
      var prevDateLastDay, firstDate, firstTime, lastDate, lastTime, lastDay, selectedTime;

      if (!this.endDate) {
        this.endDate = new Date(this.s.endDate);
        endDate.setHours(0,0,0,0);
      }

      if(this.selectedDate != -1) {
        selectedDate = new Date(settings.selectedDate);
        if((/^\d+$/).test(settings.selectedDate)) {
          selectedDate = new Date(startDate);
          selectedDate.setDate(selectedDate.getDate()+settings.selectedDate);
        }
      }
      selectedDate.setHours(0,0,0,0);
      selectedTime = selectedDate.getTime();

      // Calculate the first and last date in month being rendered.
      // Also calculate the weekday to start rendering on
      firstDate = new Date(this.date); firstDate.setDate(1);
      firstTime = firstDate.getTime();
      lastDate = new Date(firstDate); lastDate.setMonth(lastDate.getMonth()+1); lastDate.setDate(0);
      lastTime = lastDate.getTime();
      lastDay = lastDate.getDate();

      // Calculate the last day in previous month
      var prevDateLastDay = new Date(firstDate);
        prevDateLastDay.setDate(0);
        prevDateLastDay = prevDateLastDay.getDate();

      // The month names to show in toolbar
      var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

      // Render the cells as <TD>
      var days = "";
      for(var y = 0, i = 0; y < 6; y++) {
        var row = "";

        for(var x = 0; x < 7; x++, i++) {
          
          var p = ((prevDateLastDay - firstDate.getDay()) + i + 1)
            , n = p - prevDateLastDay
            , c = (x == 0) ? "sun" : ((x == 6) ? "sat" : "day");

          // If value is outside of bounds its likely previous and next months
          if(n >= 1 && n <= lastDay) {
            var today, date, dateTime;
            today = new Date(); today.setHours(0,0,0,0);
            date = new Date(this.date); date.setHours(0,0,0,0); date.setDate(n);
            dateTime = date.getTime();

            // Test to see if it's today
            c = (today.getTime() == dateTime) ? "today":c;

            // Test to see if we allow old dates
            if (!settings.allowOld) {
              c = (dateTime < startTime) ? "noday":c;
            }

            // Test against end date
            if (settings.endDate != -1) {
              c = (dateTime > endTime) ? "noday":c;
            }

            // Test against selected date
            if (settings.selectedDate != -1) {
              c = (dateTime == selectedTime) ? "selected":c;
            }
          }
          else {
            c = "noday"; // Prev/Next month dates are non-selectable by default
            n = (n <= 0) ? p : ((p - lastDay) - prevDateLastDay);
          }

          // Create the cell
          row += "<td class='gldp-days "+c+" **-"+c+"'><div class='"+c+"'>"+n+"</div></td>";
        }

        // Create the row
        days += "<tr class='days'>"+row+"</tr>";
      }

      // Determine whether to show Previous arrow
      var showP = ((startTime < firstTime) || settings.allowOld)
        , showN = ((lastTime < endTime) || (endTime < startTime));

      // Force override to showPrevNext on false
      if(!this.s.showPrevNext) { showP = showN = false; }

      // Build the html for the control
      var titleMonthYear = monthNames[this.date.getMonth()]+" "+this.date.getFullYear();
      var html =
        "<div class='**'>"+
          "<table>"+
            "<tr>"+ /* Prev Month/Year Next*/
              ("<td class='**-prevnext prev'>"+(showP ? "◄":"")+"</td>")+
              "<td class='**-monyear' colspan='5'>{MY}</td>"+
              ("<td class='**-prevnext next'>"+(showN ? "►":"")+"</td>")+
            "</tr>"+
            "<tr class='**-dow'>"+ /* Day of Week */
              "<td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td>"+
            "</tr>" + days +
          "</table>"+
        "<div>";

      // Replace css, month-year title
      html = (html.replace(/\*{2}/gi, "dp-"+settings.cssName)).replace(/\{MY\}/gi, titleMonthYear);

      // If calendar doesn't exist, make one
      if ($("#"+this.id).length == 0) {
        target.after(
          $("<div id='"+calId+"'></div>")
            .css({
              "position": this.s.position
            , "z-index":  this.s.zIndex
            , "left":     this.t.offset().left
            , "top":      this.t.offset().top + target.outerHeight(true)
            })
        );
      }

      // Show calendar
      this.calendar = $("#"+this.id);
      calendar.html(html).slideDown(200);

      // Add a class to make it easier to find when hiding
      target.addClass("_dp");

      // Handle previous/next clicks
      $("[class*=-prevnext]", calendar).bind('click', function(e) {
        e.stopPropagation();

        if ($(this).html() != "") {
          // Determine offset and set new date
          var offset = $(this).hasClass("prev") ? -1 : 1;
          var newDate = new Date(firstDate);
            newDate.setMonth(this.date.getMonth()+offset);

          // Save the new date and render the change
          target.data("this.date", newDate);
          methods.update.apply(target);
        }
      });

      // Highlight day cell on hover
      $('th.day:not(.noday), th.day:not(.selected)', this.calendar)
        .click(function(e) {
          e.stopPropagation();
          var day = $(this).children("div").html();
          var settings = target.data("settings");
          var newDate = new Date(this.date); newDate.setDate(day);

          // Save the new date and update the target control
          target.data("this.date", newDate);
          target.val((newDate.getMonth()+1)+"/"+newDate.getDate()+"/"+newDate.getFullYear());

          // Run callback to user-defined date change method
          if(settings.onChange != null && typeof settings.onChange != "undefined")
          {
            settings.onChange(target, newDate);
          }

          // Save selected
          settings.selectedDate = newDate;

          // Hide calendar
          self.hide();
        });
    }
  };

  // Plugin entry
  $.ender({
      datePicker: function(options) {
        if ($(this).data('datepicker'))
          return $(this).data('datepicker');

        var dp = new DatePicker(this, options);
        $(this).data('datepicker', dp);
        return dp;
    }
  }, this);

})(ender, window);


/*
$("#date").datePicker(
{
    allowOld: false,
    startDate: new Date("September 5, 2011"),
    endDate: new Date("October 26, 2011"),
    onChange: function(target, newDate)
    {
        alert("You selected: " + newDate);
    }
});
*/