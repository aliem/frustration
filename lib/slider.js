/*
 *  Easy Slider 1.7 - jQuery plugin
 *  written by Alen Grakalic
 *  http://cssglobe.com/post/4004/easy-slider-15-the-easiest-jquery-plugin-for-sliding
 *
 *  Copyright (c) 2009 Alen Grakalic (http://cssglobe.com)
 *  Dual licensed under the MIT (MIT-LICENSE.txt)
 *  and GPL (GPL-LICENSE.txt) licenses.
 *
 *  Built for jQuery library
 *  http://jquery.com
 *
 */

(function($) {
  // default configuration properties
  var defaults = {
    prevId: 'prevBtn',
    prevText: 'Previous',
    nextId: 'nextBtn',
    nextText: 'Next',
    controlsShow: true,
    controlsBefore: '',
    controlsAfter: '',
    controlsFade: true,
    firstId: 'firstBtn',
    firstText: 'First',
    firstShow: false,
    lastId: 'lastBtn',
    lastText: 'Last',
    lastShow: false,
    vertical: false,
    speed: 800,
    auto: false,
    pause: 2000,
    continuous: false,
    numeric: false,
    numericId: 'controls'
  };

  function Slider(el, opts) {
    this.obj = $(el);

    var self = this
      , _li = $("li", this.obj)
      , s = _li.length
      , w = _li.width()
      , h = _li.height()
      , ts = s - 1
      , t = 0;
      
    this.o = $.extend(defaults, opts);

    this.clickable = true;

    this.obj
      .width(w)
      .height(h)
      .css({
        "overflow": "hidden"
      , 'width': s*w
      });

    if (this.o.continuous) {
      this.obj
        .prepend($("li:last-child", obj).html().css("margin-left", "-" + w + "px"))
        .append($("li:nth-child(2)", obj).html())
        .css('width', (s + 1) * w);
    }

    if (!this.o.vertical) _li.css('float', 'left');

    if (this.o.controlsShow) {
      var html = this.o.controlsBefore;
      if (this.o.numeric) {
        html += '<ol id="' + this.o.numericId + '"></ol>';
      } else {
        if (this.o.firstShow) html += '<span id="' + this.o.firstId + '"><a href=\"javascript:void(0);\">' + this.o.firstText + '</a></span>';
        html += ' <span id="' + this.o.prevId + '"><a href=\"javascript:void(0);\">' + this.o.prevText + '</a></span>';
        html += ' <span id="' + this.o.nextId + '"><a href=\"javascript:void(0);\">' + this.o.nextText + '</a></span>';
        if (this.o.lastShow) html += ' <span id="' + this.o.lastId + '"><a href=\"javascript:void(0);\">' + this.o.lastText + '</a></span>';
      }

      html += this.o.controlsAfter;
      this.obj.after(html);
    }

    if (this.o.numeric) {
      for (var i = 0; i < s; i++) {
        $(document.createElement("li"))
          .attr('id', this.o.numericId + (i + 1))
          .html('<a rel=' + i + ' href=\"javascript:void(0);\">' + (i + 1) + '</a>')
          .appendTo($("#" + this.o.numericId))
          .click(function() {
            animate($("a", $(this)).attr("rel"), true);
          });
      }
    } else {
      $("a", "#" + this.o.nextId).click(function() {
        animate("next", true);
      });
      $("a", "#" + this.o.prevId).click(function() {
        animate("prev", true);
      });
      $("a", "#" + this.o.firstId).click(function() {
        animate("first", true);
      });
      $("a", "#" + this.o.lastId).click(function() {
        animate("last", true);
      });
    }

    // init
    var timeout;
    if (this.o.auto) {
      timeout = setTimeout(function() {
        animate("next", false);
      }, this.o.pause);
    }

    if (this.o.numeric) setCurrent(0);

    if (!this.o.continuous && this.o.controlsFade) {
      $("a", "#" + this.o.prevId).hide();
      $("a", "#" + this.o.firstId).hide();
    }
  }

  Slider.prototype = {
    setCurrent: function (i) {
      i = parseInt(i, 10) + 1;
      $("li", "#" + this.o.numericId).removeClass("current");
      $("li#" + this.o.numericId + i).addClass("current");
    }
  , adjust: function () {
        if (t > ts) t = 0;
        if (t < 0) t = ts;
        if (!this.o.vertical) {
          $("ul", obj).css("margin-left", (t * w * -1));
        } else {
          $("ul", obj).css("margin-left", (t * h * -1));
        }
        clickable = true;
        if (this.o.numeric) setCurrent(t);
      }
  , animate: function (dir, clicked) {
      if (clickable) {
        clickable = false;
        var ot = t;
        switch (dir) {
        case "next":
          t = (ot >= ts) ? (this.o.continuous ? t + 1 : ts) : t + 1;
          break;
        case "prev":
          t = (t <= 0) ? (this.o.continuous ? t - 1 : 0) : t - 1;
          break;
        case "first":
          t = 0;
          break;
        case "last":
          t = ts;
          break;
        default:
          t = dir;
          break;
        }
        var diff = Math.abs(ot - t);
        var speed = diff * this.o.speed;
        if (!this.o.vertical) {
          p = (t * w * -1);
          $("ul", obj).animate({
            marginLeft: p
          }, {
            queue: false,
            duration: speed,
            complete: adjust
          });
        } else {
          p = (t * h * -1);
          $("ul", obj).animate({
            marginTop: p
          }, {
            queue: false,
            duration: speed,
            complete: adjust
          });
        }

        if (!this.o.continuous && this.o.controlsFade) {
          if (t == ts) {
            $("a", "#" + this.o.nextId).hide();
            $("a", "#" + this.o.lastId).hide();
          } else {
            $("a", "#" + this.o.nextId).show();
            $("a", "#" + this.o.lastId).show();
          }
          if (t === 0) {
            $("a", "#" + this.o.prevId).hide();
            $("a", "#" + this.o.firstId).hide();
          } else {
            $("a", "#" + this.o.prevId).show();
            $("a", "#" + this.o.firstId).show();
          }
        }
        if (clicked) clearTimeout(timeout);
        if (this.o.auto && dir == "next" && !clicked) {
          timeout = setTimeout(function() {
            animate("next", false);
          }, diff * this.o.speed + this.o.pause);
        }
      }
    }
    , _: []
  }
  

  $.ender({
    slider: function(o) {
      new Slider($(this), o);
      return this;
    }
  }, true);

})(window.ender);
