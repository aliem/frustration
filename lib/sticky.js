(function($) {
  function Sticky(el) {
    var self = this;
    this.fixed = null;
    this.$nav = el;
    this.minHeight = this.$nav.offset().top;

    this.$win = $(window);

    this.processScroll();

    this.$win.bind('scroll', function(e) {
      self.processScroll(e);
    });
  }

  Sticky.prototype.processScroll = function(e) {
    var scrollTop = this.$win.scrollTop();

    if (scrollTop >= this.minHeight && !this.fixed) {
      this.fixed = true;
      this.$nav.addClass('fixed').css({
          position: 'fixed'
        , top: 0
        , 'z-index': 999
        , 'width': '100%'
      });
    } else if (scrollTop <= this.minHeight && this.fixed) {
      this.fixed = false;
      this.$nav
        .removeClass('fixed')
        .attr('style', '');
    }
  };
  $.ender({
    sticky: function() {
      $(this).data('sticky', new Sticky($(this)));
      return this;
    }
  }, true);
})(window.ender);