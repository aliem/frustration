;(function ($) {
  "use strict";

  var animation = (function (a, elm, prfx, i) {
    prfx = 'Webkit Moz O ms Khtml'.split(' ');
    elm = document.createElement('div');

    if (elm.style.animationName !== undefined) return true;

    for( var i = 0; i < prfx.length; i++ ) {
      if(elm.style[prfx[i] + 'AnimationName'] !== undefined) {
        return !a;
      }
    }
    return false;
  })();


  var _options = {
        time: 5000
      }
    , v = require('valentine')
    , transition = false;
  
  var Fadeshow = function (target, options) {
    this.target = $(target);
    this.blocks = $('li', target);
    
    this.o = v.extend(_options, options);
    if (animation) {
      v.each(this.blocks, function (i) {
          $(i).addClass('animated').addClass('fadeOut');
      })
      this.current = this.blocks.first();
      this.current.removeClass('fadeOut');
      this.current.addClass('fadeIn');
    } else {
      next(0);
    }
    this.start();
  };

  Fadeshow.prototype = {
    start: function () {
      //
      var self = this
        , l = this.blocks.length - 1
        , n = 0;

      this.interval = setInterval(function () {
        self.next(n)
        n = (n == l)? 0 : n+1;
      }, this.o.time);

      return this;
    }
  , stop: function () {
      clearInterval(this.interval);
      return this;
    }
  , next: function (n) {
      if (animation) {
        if (!! this.current) {
          this.previous = this.current;

          this.previous.removeClass('fadeIn')
          this.previous.addClass('fadeOut')
        }
        this.current = $(this.blocks[n])
        this.current.removeClass('fadeOut')
        this.current.addClass('fadeIn')
      } else {
        if (!! this.current) {
          this.previous = this.current;
          this.previous.animate({opacity: 0})
        }
        this.current = $(this.blocks[n])
        this.current.animate({opacity: 1})
      }
    }
  };

  $.ender({
    fadeshow: function (options) {
      if (this.data('fadeshow'))
        return this.data('fadeshow');
      var s = new Fadeshow(this, options);
      this.data('fadeshow', s);
      return s;
    }
  }, true);
})(window.ender);